const merge = require('webpack-merge');
const common = require('./webpack/webpack.common');
const path = require('path');
// const devServer = require('./webpack/webpack.devServer');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    historyApiFallback: false,
    host: 'localhost',
    hot: true,
    inline: true,
    port: 3000,
    // proxy: {
    //   '**': 'http://localhost:3000',
    // }
  }
});
