import React from 'react';

function ItalicMark(props) {
  return (
    <em property={'italic'}>
      {props.children}
    </em>
  );
}

export default ItalicMark;
