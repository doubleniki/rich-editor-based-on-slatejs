import React from 'react';

function FormatToolbar(props) {
  return (
    <div className={'format-toolbar'}>{props.children}</div>
  );
}

export default FormatToolbar;
