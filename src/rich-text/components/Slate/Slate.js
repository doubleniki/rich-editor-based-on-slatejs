import React, { PureComponent } from 'react';
import { Editor } from 'slate-react';
import { Value } from 'slate';

import FormatToolbar from './FormatToolbar';

import BoldMark from './BoldMark';
import ItalicMark from './ItalicMark';
import CodeBlock from './CodeBlock';

const initialValue = Value.fromJSON({
  document: {
    nodes: [
      {
        object: 'block',
        type: 'paragraph',
        nodes: [
          {
            object: 'text',
            leaves: [
              {
                text: 'A line of text in a paragraph.',
              }
              
            ]
          },
        ],
      },
    ],
  },
});



class Slate extends PureComponent {
  constructor() {
    super();
    this.onChange = ::this.onChange;
    this.renderMark = ::this.renderMark;
    this.renderBlock = ::this.renderBlock;
    this.onMarkClick = ::this.onMarkClick;
    
    this.state = {
      value: initialValue
    }
  }
  
  renderBlock(props, editor, next) {
    switch (props.node.type) {
      case 'code':
        return <CodeBlock {...props} />;
      default:
        return next();
    }
  };
  
  renderMark(props, editor , next) {
    switch (props.mark.type) {
      case 'bold':
        return <BoldMark {...props} />;
      case 'italic':
        return <ItalicMark {...props} />;
      default:
        return next();
    }
  }
  
  onMarkClick(e, mark) {
    e.preventDefault();
    const { value } = this.state;
    console.log(value);
    const change = value.toggleMark(mark);
    this.onChange(change);
  }
  
  onChange({ value }) {
    this.setState({ value })
  };
  
  onKeyDown(e, editor, next) {
    // Return with no changes if it's not the "`" key with ctrl pressed.
    if (!e.altKey) return next();
    
    // Prevent the "`" from being inserted by default.
    e.preventDefault();
  
    // Determine whether any of the currently selected blocks are code blocks.
    // const isCode = editor.value.blocks.some(block => block.type === 'code');
    
    switch (e.key) {
      case 'b': {
        editor.toggleMark('bold');
        return true
      }
      case 'c': {
        editor.setBlocks('code');
        return true
      }
      case 'i': {
        editor.toggleMark('italic');
        return true
      }
      case 'p': {
        editor.setBlocks('paragraph');
        return true
      }
      default: {
        return;
      }
    }
  };
  
  render() {
    return (
      <>
        <h3>Slate</h3>
        <div className='editor'>
          <div className="editor--controls">
            <button onClick={this.renderBlock}>code</button>
          </div>
          <FormatToolbar >
            <button
              onPointerDown={e => this.onMarkClick(e, 'bold')}
              className={'tooltip-button__bold'}
            >
              B
            </button>
            <button
              onPointerDown={e => this.onMarkClick(e, 'italic')}
              className={'tooltip-button__italic'}
            >
              I
            </button>
          </FormatToolbar>
          <div className={'editor--text'}>
            <Editor
              value={this.state.value}
              onChange={this.onChange}
              renderBlock={this.renderBlock}
              renderMark={this.renderMark}
              onKeyDown={this.onKeyDown}
            />
          </div>
        </div>
      </>
    );
  }
}

export default Slate;
