import React from 'react';
import { hot } from 'react-hot-loader/root';
import { Provider } from 'react-redux';

import App from './App/App';

const RootComponent = props => {
  return (
    <Provider store={props.store}>
      <App />
    </Provider>
  );
};

export default hot(RootComponent);
