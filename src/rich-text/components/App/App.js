import React from 'react';

import Slate from '../Slate/Slate';
import './App.scss';

export default function App() {

  return (
    <div className='page'>
      <Slate />
    </div>
  );
}
