import React from 'react';
import { render } from 'react-dom';

import RootComponent from './components/RootComponent';
import configureStore from './configureStore';

const rootElement = document.getElementById('reactRoot');
const initialState = window.__INITIAL_STATE__;
const store = configureStore({ initialState });

if (process.env.NODE_ENV !== 'production') {
  window.store = store;
}

render(
  <RootComponent store={store} />,
  rootElement
);

if (module.hot) {
  module.hot.accept('./components/RootComponent', () => {
    const NextRootComponent = require('./components/RootComponent').default;
    render(
      <NextRootComponent store={store}/>,
      rootElement
    );
  });
}
