import constants from '../constants';

const initialState = {
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constants.TEST: {
      return state;
    }

    default: {
      return state;
    }
  }
};

export default reducer;
