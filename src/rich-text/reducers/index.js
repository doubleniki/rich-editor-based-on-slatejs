import { combineReducers } from 'redux';
import richText from './richText';

export default combineReducers({
  richText,
});
